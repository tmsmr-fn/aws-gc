package function

import (
	"encoding/json"
	"fmt"
	"github.com/emersion/go-ical"
	"golang.org/x/exp/slices"
	"log"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strings"
	"time"
)

const (
	icalUrl = "https://service.stuttgart.de/lhs-services/aws/api/ical"
	tzName  = "Europe/Berlin"
)

var (
	garbageTypes = []string{"Restmüll", "Biomüll", "Altpapier", "Gelber Sack"}
	weekdays     = []string{"Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"}
	months       = []string{"", "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"}
	dateHints    = []string{"Heute", "Morgen", "Übermorgen"}
	keys         []string
)

func init() {
	val, present := os.LookupEnv("fn_api_keys")
	if present {
		keys = strings.Split(val, ",")
	}
}

type GarbageLoc struct {
	Street   string `json:"street"`
	StreetNr string `json:"streetnr"`
}

func (l GarbageLoc) fetchCal() (*ical.Calendar, error) {
	u, err := url.Parse(icalUrl)
	if err != nil {
		return nil, err
	}
	q := u.Query()
	q.Add("street", l.Street)
	q.Add("streetnr", l.StreetNr)
	u.RawQuery = q.Encode()
	r, err := http.Get(u.String())
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()
	dec := ical.NewDecoder(r.Body)
	cal, err := dec.Decode()
	if err != nil {
		return nil, err
	}
	return cal, nil
}

type CollectDate struct {
	Type    string     `json:"type"`
	Date    *time.Time `json:"date"`
	Display string     `json:"display"`
	Hint    string     `json:"hint"`
}

func findUpcomingDates(cal *ical.Calendar) ([]CollectDate, *time.Time, error) {
	tz, err := time.LoadLocation(tzName)
	if err != nil {
		return nil, nil, err
	}
	now := time.Now().In(tz)
	events := cal.Events()
	sort.SliceStable(events, func(i, j int) bool {
		ti, _ := events[i].DateTimeStart(tz)
		tj, _ := events[j].DateTimeStart(tz)
		return ti.Before(tj)
	})
	dates := make([]CollectDate, 0)
	for _, t := range garbageTypes {
		dates = append(dates, CollectDate{
			Type: t,
		})
	}
	for _, evt := range events {
		start, err := evt.DateTimeStart(tz)
		if err != nil {
			return nil, nil, err
		}
		if start.Before(now) {
			continue
		}
		complete := true
		for i, cd := range dates {
			if dates[i].Date != nil {
				continue
			}
			summ, ok := evt.Props["SUMMARY"]
			if !ok || len(summ) < 1 {
				complete = false
				continue
			}
			if strings.HasPrefix(summ[0].Value, cd.Type) {
				dates[i].Date = &start
			} else {
				complete = false
			}
		}
		if complete {
			break
		}
	}
	return dates, &now, nil
}

func stripTime(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

func Handle(w http.ResponseWriter, r *http.Request) {
	if keys != nil {
		valid := slices.Contains(keys, r.Header.Get("Fn-Api-Key"))
		if !valid {
			http.Error(w, "", http.StatusUnauthorized)
			return
		}
	}
	var loc GarbageLoc
	err := json.NewDecoder(r.Body).Decode(&loc)
	defer r.Body.Close()
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	cal, err := loc.fetchCal()
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	dates, today, err := findUpcomingDates(cal)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	for i, date := range dates {
		dates[i].Display = fmt.Sprintf("%s, %d %s", weekdays[date.Date.Weekday()], date.Date.Day(), months[date.Date.Month()])
		dToday := stripTime(*today)
		dCollect := stripTime(*date.Date)
		dd := int(dCollect.Sub(dToday).Hours() / 24)
		if dd < len(dateHints) {
			dates[i].Hint = dateHints[dd]
		}
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(dates)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
