# aws-gc
*OpenFaas function that returns upcoming garbage collection dates in Stuttgart (https://service.stuttgart.de/lhs-services/aws)*

```
curl -H "Fn-Api-Key: insecure" https://faas.example.com/function/aws-gc -d '{"street": "Königstr.", "streetnr": "1"}'
```
