package function

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCallHandle(t *testing.T) {
	loc := GarbageLoc{
		Street:   "Königstr.",
		StreetNr: "1",
	}
	body, _ := json.Marshal(loc)
	reader := bytes.NewReader(body)
	req, err := http.NewRequest("POST", "/", reader)
	if err != nil {
		t.Fatal(err)
	}
	//req.Header.Add("Fn-Api-Key", "insecure")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Handle)
	handler.ServeHTTP(rr, req)
	if rr.Code != http.StatusOK {
		t.Fatal(err)
	}
	var dates []CollectDate
	err = json.NewDecoder(rr.Body).Decode(&dates)
	if rr.Code != http.StatusOK {
		t.Fatal(err)
	}
}
