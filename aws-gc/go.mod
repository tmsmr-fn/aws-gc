module handler/function

go 1.19

require (
	github.com/emersion/go-ical v0.0.0-20220601085725-0864dccc089f
	golang.org/x/exp v0.0.0-20230304125523-9ff063c70017
)

require github.com/teambition/rrule-go v1.7.2 // indirect
